//
//  TableViewCell.swift
//  desafio-ios-Vitor-Zitti
//
//  Created by Vitor Zitti on 5/1/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class TableViewCell : UITableViewCell {
    
    
    var gameInformation : TwitchApiJson?

    @IBOutlet weak var gameImage: UIImageView!
    @IBOutlet weak var gameName: UILabel!
    @IBOutlet weak var ranking: UILabel!
    @IBOutlet weak var viewers: UILabel!
    
    func SetUpCell(){
        //Load all the game information
        let game = gameInformation?.game
        let box = game?.box
        let imageLink = box?.medium
        self.gameName.text = game?.name
        self.viewers.text = String(gameInformation!.viewers) + " Viewers"
        do {
            let imageFound = try UIImage(data: Data(contentsOf: URL(string: imageLink!)!))
            if let image = imageFound {
                self.gameImage.image = image
            }
        } catch {
            print("ERROR on IMAGE LOAD")
        }
    }
}
