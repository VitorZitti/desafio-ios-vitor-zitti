# Aplicativo de consulta na api do [Twitch.tv](https://www.twitch.tv/) #

## Projeto:

* Storyboard e Autolayout
* Comunicação com a API da Twitch.tv.
* Codable para o mapeamento json.

## Tela de Listagem:

* Lista de [jogos](https://dev.twitch.tv/docs/v5/reference/games/) com nome do jogo, foto e ranking
* Scroll infinito na tela de lista.
* Detecta o fim da paginação.
* Pull to refresh.


## Tela de Detalhe:

* Tela de detalhe ao clicar em um item da lista.
* Tela de detalhe contem nome do jogo, foto e quantidade de espectadores