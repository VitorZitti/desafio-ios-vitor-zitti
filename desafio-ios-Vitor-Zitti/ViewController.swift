//
//  ViewController.swift
//  desafio-ios-Vitor-Zitti
//
//  Created by Vitor Zitti on 5/1/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    //Twitch API Url for the top games
    let urlTwitchApiBasic : String = "https://api.twitch.tv/kraken/games/top?client_id=z5rnrfzwz2l851dyc3yzv1kgkygfuj&limit="
    
    var twitchLimit : Int = 20
    var twitchOffset : Int = 0
    var stopUpdating : Bool = false
    
    //The array of Structs for the games information
    var topGames : [TwitchApiJson] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addSubview(self.refreshControl)
        self.tableView.rowHeight = 105
        LoadTopGamesData()
    }
    
    func LoadTopGamesData(){
        //Alamofire Solution
        print("Loading the data")
        let urlTwitchApi = URL(string: urlTwitchApiBasic + String(self.twitchLimit) + "&offset=" + String(self.twitchOffset))
        Alamofire.request(urlTwitchApi!).validate().response { (response) in
            if let data = response.data {
                //Print the data we got and check if it contains an Error
                let str = String(data: data, encoding: .utf8)!
                //print(str)
                if str.range(of: "Bad Request") == nil{
                    print("Request Completed")
                    do{
                        let objectJson = try JSONDecoder().decode(JsonTop.self, from: data)
                        print("JSONSerialization")
                        //Get the games as a Dictionary
                        self.topGames += objectJson.top!
                        if self.twitchOffset == 0 {
                            self.twitchOffset = self.twitchLimit - 1
                        }
                        
                        //Get the data then load the TableView
                        self.tableView.reloadData()
                    } catch{
                        print("Decoder error")
                    }
                } else {
                    print("Stop Updating")
                    self.stopUpdating = true
                }
            }
        }
        print("Loading Data Ended")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    //Pull to refresh
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ViewController.handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = UIColor.white
        return refreshControl
    }()
    
    //Reset the control variables when refreshing
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.stopUpdating = false
        self.twitchOffset = 0
        self.topGames = []
        LoadTopGamesData()
        refreshControl.endRefreshing()
    }
    
    //Table View DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(self.topGames.count)
        return self.topGames.count
    }
    
    //Check if the user pressed the Cell then load the new screen
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "GameInformation") as! GameInformationViewController
            vc.gameInformation = self.topGames[indexPath.row]
            vc.gameRank = indexPath.row+1
            self.present(vc, animated: true, completion: nil)
    }
    
    //Create the cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->   UITableViewCell {
        
        let gameCell = tableView.dequeueReusableCell(withIdentifier: "gameTableViewCell", for: indexPath) as! TableViewCell

        if self.twitchOffset > 0{
            if self.stopUpdating == false && indexPath.row >= self.twitchOffset - (self.twitchLimit/2){
                self.twitchOffset += self.twitchLimit
                LoadTopGamesData()
            }
            gameCell.gameInformation = self.topGames[indexPath.row]

            gameCell.ranking.text = "#" + String(indexPath.row+1)
        //Load all the game information on Cell Creating
            gameCell.SetUpCell()
        }
        return gameCell
    }
}


//Codables for the Json
struct JsonTop : Codable{
    let top : [TwitchApiJson]?
}

struct TwitchApiJson : Codable {
    let game: GameInformation
    let viewers : Int
    let channels : Int
}

struct GameInformation : Codable{
    let name : String
    let popularity : Int
    let box : GameBoxImage
}

struct GameBoxImage : Codable{
    let small : String
    let medium : String
    let large : String
}
