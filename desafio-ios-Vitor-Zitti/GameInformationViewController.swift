//
//  GameInformationViewController.swift
//  desafio-ios-Vitor-Zitti
//
//  Created by Vitor Zitti on 5/1/18.
//  Copyright © 2018 Zitti. All rights reserved.
//

import Foundation
import UIKit

class GameInformationViewController: UIViewController{
    var gameInformation : TwitchApiJson?
    var gameRank : Int = 0
    
    @IBOutlet weak var gameNameLabel: UILabel!
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var gameRankLabel: UILabel!
    @IBOutlet weak var gameViewersLabel: UILabel!
    @IBOutlet weak var channelsLabel: UILabel!

    override func viewDidLoad() {
        
        //Load all the game information
        self.gameRankLabel.text = "#" + String(gameRank) + " in popularity"
        
        let game = gameInformation?.game
        self.gameNameLabel.text = game?.name
        self.channelsLabel.text = String(gameInformation!.channels ) + " Channels"
        self.gameViewersLabel.text = String(gameInformation!.viewers ) + " Viewers"
        
        let box = game!.box 
        let imageLink = box.large
        do {
            let imageFound = try UIImage(data: Data(contentsOf: URL(string: imageLink)!))
            if let image = imageFound {
                self.gameImageView.image = image
            }
        } catch {
            print("ERROR on IMAGE LOAD")
        }
    }
}
